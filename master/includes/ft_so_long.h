/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_so_long.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/18 11:37:02 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/02 01:42:55 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SO_LONG_H
# define FT_SO_LONG_H

# include "libft.h"
# include "ft_struct.h"
# include "ft_key.h"
# include "mlx.h"
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

# define H	800
# define W	800
# define FONT_G "-misc-fixed-medium-r-normal--13-120-75-75-c-70-iso8859-1"
# define EXT "ber"

int		parsing_map(t_data *data, char *filemap);
int		check_map(t_data *data);

void	create_map(t_data *data);

int		find_player(t_data *data, char value);
int		find_coin(t_data *data, char value);
int		find_exit(t_data *data, char value);
int		find_value(char c);

int		map_is_close(t_data *data);
int		map_is_correct(t_data *data);

void	print_error_message(char *msg);

int		init_so_long(t_data *data);
int		init_img(t_data *data);
int		init_textures(t_data *data);

void	put_pixel_in_img(t_data *d, int x, int y, int color);
void	show_counter(t_data *data);

void	setup_hook(t_data *data);
int		keypress(int keycode, t_data *data);

void	game_start(t_data *d);
int		game_loop(t_data *d);

void	move_player(t_data *data, int x, int y);

void	free_map(t_data *data);

int		end_so_long(t_data *data);
void	exit_usage(void);
void	exit_init(t_data *data);
void	exit_so_long(void);

#endif
