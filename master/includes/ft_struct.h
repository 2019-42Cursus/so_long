/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_struct.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/21 09:29:06 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/01 13:46:28 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRUCT_H
# define FT_STRUCT_H

# include "ft_error.h"

typedef struct s_color
{
	unsigned int	r;
	unsigned int	g;
	unsigned int	b;
}				t_color;

typedef struct s_location {
	int	x;
	int	y;
}			t_location;

typedef struct s_player {
	int			has_player;
	t_location	location;
	int			coin;
	int			move;
}				t_player;

typedef struct s_m_data {
	void		*img;
	char		*addr;
	int			bpp;
	int			line_len;
	int			endian;
	int			height;
	int			width;
	char		*path;
}			t_m_data;

typedef struct s_map
{
	int			fd;
	int			tmp;
	char		**values;
	int			is_valid;
	int			coin_nbr;
	int			exit_x;
	int			exit_y;
	int			has_exit;
	int			height;
	int			width;
	t_m_data	avatar;
	t_m_data	exit;
	t_m_data	wall;
	t_m_data	floor;
	t_m_data	coin;
	t_player	player;
}				t_map;

typedef struct s_image {
	void	*img;
	char	*addr;
	int		line_len;
	int		bpp;
	int		endian;
	int		width;
	int		height;
}				t_image;

typedef struct s_mlx
{
	void	*mlx;
	void	*win;
}				t_mlx;

typedef struct s_data
{
	t_mlx		mlx;
	t_image		image;
	t_map		map;
}				t_data;

#endif
