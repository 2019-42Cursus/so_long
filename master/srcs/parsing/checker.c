/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/20 10:18:49 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/02 01:28:13 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

static int
	map_has_coin(t_data *data)
{
	return (find_coin(data, 'C'));
}

static int
	map_has_player(t_data *data)
{
	if (!find_player(data, 'P') || !data->map.player.has_player)
		return (FALSE);
	return (TRUE);
}

static int
	map_has_exit(t_data *data)
{
	if (!find_exit(data, 'E') || !data->map.has_exit)
		return (FALSE);
	return (TRUE);
}

int
	check_map(t_data *data)
{
	static int		(*pf[])(t_data *data) = {map_is_correct,
		map_has_player, map_has_exit, map_is_close, map_has_coin};
	static char		*error_message[5] = {INVALID_SIZE, NO_PLAYER, NO_EXIT,
		MAP_IS_OPEN, NO_COIN};
	unsigned long	index;

	index = 0;
	create_map(data);
	data->map.is_valid = TRUE;
	while (index < sizeof(pf) / sizeof(*pf))
	{
		if (!pf[index](data))
		{
			print_error_message(error_message[index]);
			close(data->map.fd);
			data->map.is_valid = FALSE;
			break ;
		}
		index++;
	}
	return (data->map.is_valid);
}
