/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/01 08:04:05 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/01 08:15:13 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

int
	map_is_close(t_data *data)
{
	int	x;
	int	y;

	y = -1;
	while (++y < data->map.height)
	{
		if (y == 0 || y == data->map.height - 1)
		{
			x = -1;
			while (++x < data->map.width)
			{
				if (data->map.values[y][x] != '1')
					return (FALSE);
			}
		}
		else
		{
			if (data->map.values[y][0] != '1'
				|| data->map.values[y][data->map.width - 1] != '1')
				return (FALSE);
		}
	}
	return (TRUE);
}

int
	map_is_correct(t_data *data)
{
	int	i;
	int	x;

	i = 0;
	data->map.width = ft_strlen(data->map.values[0]);
	while (++i < data->map.height)
	{
		if ((int)ft_strlen(data->map.values[i]) != data->map.width)
			return (FALSE);
		x = -1;
		while (++x < data->map.width)
		{
			if (!find_value(data->map.values[i][x]))
				return (FALSE);
		}
	}
	data->image.width = data->map.width * 40;
	data->image.height = data->map.height * 40;
	return (TRUE);
}
