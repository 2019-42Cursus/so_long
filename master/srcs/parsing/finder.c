/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   finder.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/25 11:41:04 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/01 08:09:53 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

int
	find_value(char c)
{
	if (c == 'P')
		return (TRUE);
	if (c == 'E')
		return (TRUE);
	if (c == 'C')
		return (TRUE);
	if (c == '1')
		return (TRUE);
	if (c == '0')
		return (TRUE);
	return (FALSE);
}

int
	find_player(t_data *data, char value)
{
	int	x;
	int	y;
	int	error;

	error = FALSE;
	y = 0;
	while (++y < data->map.height)
	{
		x = 0;
		while (x++ < data->map.width)
		{
			if (data->map.values[y][x] == value)
			{
				if (data->map.player.has_player)
					error = TRUE;
				data->map.player.location.x = x;
				data->map.player.location.y = y;
				data->map.player.has_player = TRUE;
			}
		}
	}
	return (!error);
}

int
	find_exit(t_data *data, char value)
{
	int	x;
	int	y;
	int	error;

	error = FALSE;
	y = 0;
	while (++y < data->map.height)
	{
		x = 0;
		while (x++ < data->map.width)
		{
			if (data->map.values[y][x] == value)
			{
				if (data->map.has_exit)
					error = TRUE;
				data->map.has_exit = TRUE;
			}
		}
	}
	return (!error);
}

int
	find_coin(t_data *data, char value)
{
	int	x;
	int	y;

	y = 0;
	while (++y < data->map.height)
	{
		x = 0;
		while (x++ < data->map.width)
		{
			if (data->map.values[y][x] == value)
				data->map.coin_nbr++;
		}
	}
	if (data->map.coin_nbr > 0)
		return (TRUE);
	return (FALSE);
}
