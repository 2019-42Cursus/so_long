/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/18 12:28:59 by thzeribi          #+#    #+#             */
/*   Updated: 2022/01/29 21:40:02 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

static int
	open_map(t_data *data, char *filemap)
{
	int	fd;
	int	tmp;

	fd = open(filemap, O_DIRECTORY);
	if (fd != -1)
	{
		print_error_message(MAP_IS_DIRECTORY);
		close(fd);
		return (FALSE);
	}
	fd = open(filemap, O_RDONLY | O_NOFOLLOW);
	tmp = open(filemap, O_RDONLY | O_NOFOLLOW);
	if (fd == -1 || tmp == -1)
	{
		print_error_message(INVALID_FILE);
		return (FALSE);
	}
	data->map.fd = fd;
	data->map.tmp = tmp;
	return (TRUE);
}

static int
	check_map_name(char *filemap)
{
	const char	*ext;

	if (filemap[0] == '\0')
		return (FALSE);
	ext = ft_file_ext(filemap);
	if (ext != NULL && ft_strcmp(ext, EXT) == 0)
		return (TRUE);
	print_error_message(INVALID_EXT);
	return (FALSE);
}

int
	parsing_map(t_data *data, char *filemap)
{
	data->map.is_valid = TRUE;
	if (!check_map_name(filemap))
		return (FALSE);
	if (!open_map(data, filemap))
		return (FALSE);
	if (!check_map(data))
		return (FALSE);
	return (TRUE);
}
