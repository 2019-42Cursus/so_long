/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/31 06:52:22 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/02 01:48:28 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

void
	show_counter(t_data *data)
{
	char	*move;
	char	*coin;

	move = ft_itoa(data->map.player.move);
	coin = ft_itoa(data->map.player.coin);
	mlx_string_put(data->mlx.mlx, data->mlx.win, 5, 10, 0x000d9e, "Move: ");
	mlx_string_put(data->mlx.mlx, data->mlx.win, 35, 10, 0x000d9e, move);
	mlx_string_put(data->mlx.mlx, data->mlx.win, 5, 30, 0xD1A400, "Coin: ");
	mlx_string_put(data->mlx.mlx, data->mlx.win, 35, 30, 0xD1A400, coin);
	free(move);
	free(coin);
}

static void
	new_pos(t_data *data, int x, int y)
{
	data->map.player.move++;
	write(1, "\r", 1);
	ft_putnbr(data->map.player.move);
	data->map.values[data->map.player.location.y]
	[data->map.player.location.x] = '0';
	data->map.player.location = (t_location){.x = x, .y = y};
	data->map.values[y][x] = 'P';
}

void
	move_player(t_data *data, int x, int y)
{
	if (data->map.values[y][x] == '1')
		return ;
	if (data->map.values[y][x] == 'E')
	{
		if (data->map.player.coin >= data->map.coin_nbr)
			end_so_long(data);
	}
	else if (data->map.values[y][x] == '0')
		new_pos(data, x, y);
	else if (data->map.values[y][x] == 'C')
	{
		data->map.player.coin++;
		new_pos(data, x, y);
	}
}
