/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/30 05:59:42 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/01 13:55:34 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

t_m_data
	*choose_texture(t_data *data, char t)
{
	if (t == 'P')
		return (&data->map.avatar);
	if (t == 'E')
		return (&data->map.exit);
	if (t == 'C')
		return (&data->map.coin);
	if (t == '1')
		return (&data->map.wall);
	if (t == '0')
		return (&data->map.floor);
	return (NULL);
}

void	draw_image(t_data *data)
{
	t_location	location;
	t_m_data	*m_data;

	location.y = 0;
	while (location.y < data->image.height)
	{
		location.x = 0;
		while (location.x < data->image.width)
		{
			m_data = choose_texture(data,
					data->map.values[location.y / 40][location.x / 40]);
			if (location.x % 40 == 0 && location.y % 40 == 0)
				mlx_put_image_to_window(data->mlx.mlx, data->mlx.win,
					m_data->img, location.x, location.y);
			location.x++;
		}
		location.y++;
	}
}

int
	game_loop(t_data *data)
{
	draw_image(data);
	show_counter(data);
	return (TRUE);
}

void
	game_start(t_data *d)
{
	draw_image(d);
}
