/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/25 14:06:39 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/01 13:14:56 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

void
	print_map(t_data *data)
{
	int	i;

	i = 0;
	printf("MAP :\n");
	while (i < data->map.height)
	{
		printf("%s\n", data->map.values[i]);
		i++;
	}
}

static int
	add_line(t_data *data, char *line, int i)
{
	data->map.values[i] = ft_strdup(line);
	if (!data->map.values[i])
		return (FALSE);
	return (TRUE);
}

static int
	calc_height(t_data *data)
{
	int		read;
	char	*line;
	int		count;

	read = 1;
	count = 0;
	line = NULL;
	while (read > 0)
	{
		read = get_next_line(data->map.tmp, &line);
		if (line != NULL && ft_strlen(line) > 0)
		{
			count++;
			ft_memdel(&line);
		}
	}
	ft_memdel(&line);
	close(data->map.tmp);
	return (count);
}

void
	create_map(t_data *data)
{
	int		read;
	char	*line;
	int		i;

	read = 1;
	i = -1;
	line = NULL;
	data->map.height = calc_height(data);
	if (data->map.height <= 0)
		exit_init(data);
	data->map.values = (char **)malloc(sizeof(char *) * (data->map.height));
	if (data->map.values == NULL)
		end_so_long(data);
	while (read > 0)
	{
		read = get_next_line(data->map.fd, &line);
		if (line != NULL && ft_strlen(line) > 0)
		{
			add_line(data, line, ++i);
			ft_memdel(&line);
		}
	}
	ft_memdel(&line);
	close(data->map.fd);
}

void
	free_map(t_data *data)
{
	int		i;

	i = 0;
	while (i < data->map.height)
	{
		free(data->map.values[i]);
		data->map.values[i] = NULL;
		i++;
	}
	free(data->map.values);
	data->map.values = NULL;
}
