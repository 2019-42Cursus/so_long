/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_so_long_img.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 05:44:08 by thzeribi          #+#    #+#             */
/*   Updated: 2022/01/18 11:42:29 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

/**
** @name put_pixel_in_img();
** @brief This function put colored pixel to a img.
** Shifting (x * f->img.bpp >> 3) is a binary
** operation equivalent to a division by 8,
** but shifting is more faster.
**
** @param t_data *d
** @param int *x
** @param int *y
** @param int *color
**/
void
	put_pixel_in_img(t_data *d, int x, int y, int color)
{
	if (x >= 0 && y >= 0 && x < W && y < H)
		*(int *)&d->image.addr[(x * d->image.bpp >> 3) +
			(y * d->image.line_len)] = color;
}
