/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_event.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 05:37:57 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/01 13:46:28 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

/**
** @name keypress();
** @brief Call when key is pressed. This function is init
** by mlx_key_hook() from MLX Lib in setup_hook() function.
**
** @param int keycode
** @param t_data *data
** @return keycode
**/
int
	keypress(int keycode, t_data *d)
{
	int	x;
	int	y;

	x = d->map.player.location.x;
	y = d->map.player.location.y;
	if (keycode == ESC)
		end_so_long(d);
	else if (keycode == UP_ARROW)
		move_player(d, x, --y);
	else if (keycode == DOWN_ARROW)
		move_player(d, x, ++y);
	else if (keycode == LEFT_ARROW)
		move_player(d, --x, y);
	else if (keycode == RIGHT_ARROW)
		move_player(d, ++x, y);
	return (keycode);
}
