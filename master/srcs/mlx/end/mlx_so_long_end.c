/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_so_long_end.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 05:41:02 by thzeribi          #+#    #+#             */
/*   Updated: 2022/01/30 06:03:17 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

/**
** @name init_so_long()
** @brief this function is called at the very end of the program. His goal
** is to free all allocated memory.
**
** @param t_data *data
** @return TRUE [1]
** @return FALSE [0]
**/
int
	end_so_long(t_data *data)
{
	if (data->mlx.mlx)
	{
		if (data->image.img)
			mlx_destroy_image(data->mlx.mlx, data->image.img);
		if (data->map.avatar.img)
			mlx_destroy_image(data->mlx.mlx, data->map.avatar.img);
		if (data->map.exit.img)
			mlx_destroy_image(data->mlx.mlx, data->map.exit.img);
		if (data->map.floor.img)
			mlx_destroy_image(data->mlx.mlx, data->map.floor.img);
		if (data->map.wall.img)
			mlx_destroy_image(data->mlx.mlx, data->map.wall.img);
		if (data->map.coin.img)
			mlx_destroy_image(data->mlx.mlx, data->map.coin.img);
		if (data->mlx.win)
			mlx_destroy_window(data->mlx.mlx, data->mlx.win);
		mlx_loop_end(data->mlx.mlx);
		mlx_destroy_display(data->mlx.mlx);
	}
	free_map(data);
	exit_so_long();
	return (0);
}
