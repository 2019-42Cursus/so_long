/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 05:35:19 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/01 08:28:18 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

/**
** @name setup_hook();
** @brief Call by init_so_long() function. This function init all hook event.
**
** @param t_data *data
**/
void
	setup_hook(t_data *data)
{
	mlx_key_hook(data->mlx.win, keypress, data);
	mlx_hook(data->mlx.win, 33, 1L << 17, end_so_long, data);
}
