/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_textures.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/25 16:41:23 by thzeribi          #+#    #+#             */
/*   Updated: 2022/01/31 06:48:48 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

static int
	load_texture(t_data *data, char *path, t_m_data	*m)
{
	m->img = mlx_xpm_file_to_image(data->mlx.mlx, path, &m->width, &m->height);
	if (m->img == NULL)
		return (FALSE);
	m->addr = mlx_get_data_addr(m->img, &m->bpp, &m->line_len, &m->endian);
	if (m->addr == NULL)
		return (FALSE);
	m->path = path;
	return (TRUE);
}

int
	init_textures(t_data *data)
{
	if (!load_texture(data, "textures/player.xpm", &data->map.avatar))
		return (FALSE);
	if (!load_texture(data, "textures/exit.xpm", &data->map.exit))
		return (FALSE);
	if (!load_texture(data, "textures/wall.xpm", &data->map.wall))
		return (FALSE);
	if (!load_texture(data, "textures/floor.xpm", &data->map.floor))
		return (FALSE);
	if (!load_texture(data, "textures/coin.xpm", &data->map.coin))
		return (FALSE);
	return (TRUE);
}
