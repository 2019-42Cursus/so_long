/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/18 11:36:20 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/01 06:02:10 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_so_long.h"

/*
** WIP: Check if map is close + check if map contains incorrect values
*/

/**
** @name main();
** @brief This function is the main function.
**
** @param int argc
** @param char *argv[]
**/
int
	main(int argc, char *argv[])
{
	t_data	data;

	data = (t_data){0};
	if (argc != 2)
		exit_usage();
	if (!parsing_map(&data, argv[1]))
		end_so_long(&data);
	if (!init_so_long(&data) || !init_img(&data) || !init_textures(&data))
		exit_init(&data);
	game_start(&data);
	mlx_loop(data.mlx.mlx);
	return (0);
}
